package model

import "github.com/jinzhu/gorm"

// Tag used for File and Version
type Tag struct {
	Name string `gorm:"PRIMARY_KEY"`
}

// Save - save tags
func (tag *Tag) Save(db *gorm.DB) error {
	if !db.NewRecord(tag) {
		return db.Create(tag).Error
	}
	return nil
}
