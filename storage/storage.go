package storage

import (
	"bufio"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"path/filepath"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/atomikin/file-server/common/config"
)

// FileStorage - file storage interface
type FileStorage interface {
	// Append a new chunk
	Append(uuid.UUID, uint64, io.Reader) (int64, error)
	// Complete - calculate sha256 sum and if match provided string, complete the file.
	Complete(uuid.UUID, string) error
	//ReadCloser - get a ReadCloser for provided versionID. Error for missing files.
	ReadCloser(uuid.UUID) (io.ReadCloser, error)
	// Delete file from storage, error for missing files. Incomplete files are also deleted.
	Delete(uuid.UUID) error
}

// Storage is DAO for saving files
// {version_id}.part - incomplete files, {version_id} - complete file
type Storage struct {
	configuration *config.StorageConf
}

// NewFSStorage creates new File System Storage
func NewFSStorage(cfg *config.StorageConf) (FileStorage, error) {
	fsStorage := new(Storage)
	fsStorage.configuration = cfg
	if _, err := os.Stat(cfg.Path); err == nil {
		return fsStorage, nil
	} else if os.IsNotExist(err) {
		return fsStorage, os.Mkdir(cfg.Path, 0700)
	} else {
		return nil, fmt.Errorf("%s is not a diretory", cfg.Path)
	}
}

func (storage *Storage) filePath(versionID uuid.UUID) string {
	name := filepath.Join(storage.configuration.Path, versionID.String())
	if _, err := os.Stat(name); err != nil {
		name += ".part"
	}
	return name
}

func (storage *Storage) hashFile(versionID uuid.UUID) (string, error) {
	hasher := sha256.New()
	reader, err := storage.ReadCloser(versionID)
	if err != nil {
		return "", err
	}
	defer reader.Close()
	buffer := bufio.NewReader(reader)
	_, err = io.Copy(hasher, buffer)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(hasher.Sum(nil)), nil
}

// Append implementation
func (storage *Storage) Append(versionID uuid.UUID, offset uint64, payload io.Reader) (int64, error) {
	file, err := os.OpenFile(
		storage.filePath(versionID),
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644,
	)
	if err != nil {
		return 0, err
	}
	buffer := bufio.NewWriter(file)
	defer file.Close()
	defer buffer.Flush()
	return io.Copy(buffer, payload)
}

// ReadCloser - return readcloser for a file
func (storage *Storage) ReadCloser(versionID uuid.UUID) (io.ReadCloser, error) {
	path := storage.filePath(versionID)
	if _, err := os.Stat(path); err != nil {
		return nil, err
	}
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	return file, nil
}

// Complete file upload
func (storage *Storage) Complete(versionID uuid.UUID, hash string) error {
	currentHash, err := storage.hashFile(versionID)
	path := storage.filePath(versionID)
	if err != nil {
		return err
	}
	if currentHash != hash {
		return fmt.Errorf(
			fmt.Sprintf("%s - broken file, hashed don't match", versionID.String()),
		)
	}
	newPath := path[0 : len(path)-len(filepath.Ext(path))]
	err = os.Rename(path, newPath)
	if err != nil {
		return fmt.Errorf("Cannot rename %s -> %s: %s", path, newPath, err.Error())
	}
	return nil
}

// Delete file from storage
func (storage *Storage) Delete(versionID uuid.UUID) error {
	path := storage.filePath(versionID)
	if _, err := os.Stat(path); err == nil {
		err = os.Remove(path)
		if err != nil {
			return fmt.Errorf("Cannot remove file %s: %s", path, err.Error())
		}
	}
	return nil
}
