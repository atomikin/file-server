package representation

import (
	"fmt"
	"reflect"
	"time"

	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/atomikin/file-server/api/model"
)

const (
	fileType           = "file"
	fileCollectionType = "files"
	msgVersion         = 1
)

// File - file object for communications with client
type File struct {
	ID        uuid.UUID `json:"id"`
	Name      string    `json:"name"`
	Folder    string    `json:"folder"`
	Tags      []string  `json:"tags"`
	CreatedAt time.Time `json:"created_at"`
}

// FileMsg -
type FileMsg struct {
	BaseMsg
	Data File `json:"data"`
}

// FileCollection -
type FileCollection struct {
	BaseCollectionMsg
	Data []File `json:"data"`
}

// FromModel - read data
func (file *FileMsg) FromModel(target interface{}) error {
	fl, ok := target.(*model.File)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(target).Name())
	}
	file.Type = fileType
	file.Version = msgVersion

	file.Type = fileType

	file.Data.ID = fl.ID
	file.Data.Folder = fl.Folder
	file.Data.Name = fl.Name
	file.Data.CreatedAt = fl.CreatedAt
	tags := make([]string, 0, len(fl.Tags))
	if fl.Tags != nil {
		for _, t := range fl.Tags {
			tags = append(tags, t.Name)
		}
	}
	file.Data.Tags = tags
	return nil
}

// ToModel -
func (file *FileMsg) ToModel(target interface{}) error {
	fileDB, ok := target.(*model.File)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(target).Name())
	}
	nullID := uuid.UUID{}
	if file.Data.ID != nullID {
		fileDB.ID = file.Data.ID
	}
	if file.Data.Name != "" {
		fileDB.Name = file.Data.Name
	}
	if file.Data.Folder != "" {
		fileDB.Folder = file.Data.Folder
	}
	tags := make([]model.Tag, 0, len(file.Data.Tags))
	if file.Data.Tags != nil {
		for _, t := range file.Data.Tags {
			tags = append(tags, model.Tag{Name: t})
		}
	}
	fileDB.Tags = tags
	return nil
}

// Verify -
func (file *FileMsg) Verify() error {
	if file.Version != msgVersion {
		return errors.Errorf("Wrong version %s", file.Version)
	}
	if file.Type != fileType {
		return errors.Errorf("Wrong message type %s", file.Type)
	}
	if file.Data.Folder == "" {
		return errors.New("File folder is required")
	}
	if file.Data.Name == "" {
		return errors.New("File name is required")
	}
	return nil
}

//FromModel -
func (files *FileCollection) FromModel(target interface{}) error {
	files.Version = msgVersion
	files.Type = fileCollectionType
	models, ok := target.(model.Files)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(target).Name())
	}
	files.Data = make([]File, 0, len(models))
	for _, model := range models {
		var fileMsg FileMsg
		err := fileMsg.FromModel(&model)
		if err != nil {
			return err
		}
		files.Data = append(files.Data, fileMsg.Data)
	}
	files.Count = len(files.Data)
	return nil
}

// ToModel -
func (files *FileCollection) ToModel(target interface{}) error {
	return nil
}

// Verify -
func (files *FileCollection) Verify() error {
	return nil
}
