package storage

import (
	"bufio"
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"testing"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/atomikin/file-server/common/config"
)

func hashFile(path string) (string, error) {
	hasher := sha256.New()
	file, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer file.Close()
	buffer := bufio.NewReader(file)
	_, err = io.Copy(hasher, buffer)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(hasher.Sum(nil)), nil
}

func testStorage(cfg *config.Configuraton) (FileStorage, error) {
	storage, err := NewFSStorage(&cfg.Storage)
	if err != nil {
		return nil, err
	}
	return storage, nil
}

func testFile() (string, error) {
	path := "/tmp/storage_test"
	file, err := os.Create(path)
	if err != nil {
		return "", err
	}
	defer file.Close()
	buffer := bufio.NewWriter(file)
	defer buffer.Flush()
	for i := 0; i < 1000; i++ {
		buffer.Write([]byte("This is test text\n"))
	}
	return path, nil
}

func TestAppend(t *testing.T) {
	cfg := config.Load(
		fmt.Sprintf("%s/src/gitlab.com/atomikin/file-server/config.yml", os.Getenv("GOPATH")),
	)
	targetFile, _ := testFile()
	hash, err := hashFile(targetFile)
	if err != nil {
		t.Fatal(err.Error())
	}
	storage, err := testStorage(cfg)
	if err != nil {
		t.Fatal(err.Error())
	}
	file, err := os.Open(targetFile)
	if err != nil {
		t.Fatal(err.Error())
	}
	id := uuid.NewV4()
	buffer := make([]byte, 10)
	var offset int64 = 0
	for {
		fmt.Println(offset)
		n, err := file.Read(buffer)
		if n == 0 || err != nil {
			break
		}
		m, err := storage.Append(id, uint64(offset), bytes.NewBuffer(buffer[:n]))
		offset += m
		if m != int64(n) {
			t.Fatalf("Inserted %d intead of %d", m, n)
		}
	}
	err = storage.Complete(id, hash)
	if err != nil {
		t.Fatal(err.Error())
	}
	newHash, err := hashFile(filepath.Join(cfg.Storage.Path, id.String()))
	if err != nil {
		t.Fatal(err.Error())
	}
	if newHash != hash {
		t.Fatalf("%s(new) != %s(old)", newHash, hash)
	}

	err = storage.Delete(id)
	if err != nil {
		t.Fatal(err.Error())
	}
	if _, err := os.Stat(filepath.Join(cfg.Storage.Path, id.String())); err == nil {
		t.Fatalf("File %s was not deleted", id.String())
	}
}
