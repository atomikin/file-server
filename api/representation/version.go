package representation

import (
	"fmt"
	"log"
	"reflect"
	"time"

	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/atomikin/file-server/api/model"
)

const (
	versionType           = "version"
	versionCollectionType = "versions"
	versionVersion        = 1
)

// Version -
type Version struct {
	ID        uuid.UUID `json:"id"`
	CreatedAt time.Time `json:"created_at"`
	FileName  string    `json:"file_name"`
	// StorageID    uuid.UUID `json:`
	RelFile      uuid.UUID `json:"file_id"`
	SHA256Hash   string    `json:"sha256"`
	Size         uint64    `json:"size"`
	UploadedSize uint64    `json:"uploaded_size"`
	Verified     bool      `json:"verified"`
	Tags         []string  `json:"tags"`
}

// VersionMsg -
type VersionMsg struct {
	BaseMsg
	Data Version `json:"data"`
}

// VersionCollection -
type VersionCollection struct {
	BaseCollectionMsg
	Data []Version `json:"data"`
}

// FromModel - Representation impl
func (version *VersionMsg) FromModel(target interface{}) error {
	dbModel, ok := target.(*model.Version)
	version.Version = versionVersion
	version.Type = versionType
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(target).Name())
	}
	version.Data.ID = dbModel.ID
	version.Data.CreatedAt = dbModel.CreatedAt
	version.Data.FileName = dbModel.FileName
	version.Data.RelFile = dbModel.RelFile
	version.Data.SHA256Hash = dbModel.SHA256Hash
	version.Data.Size = dbModel.Size
	version.Data.UploadedSize = dbModel.UploadedSize
	version.Data.Verified = dbModel.Verified
	tags := make([]string, 0)
	if dbModel.Tags != nil {
		for _, tag := range dbModel.Tags {
			tags = append(tags, tag.Name)
		}
	}
	version.Data.Tags = tags
	return nil
}

// ToModel - Representation impl
func (version *VersionMsg) ToModel(target interface{}) error {
	dbModel, ok := target.(*model.Version)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(target).Name())
	}
	dbModel.FileName = version.Data.FileName
	if dbModel.SHA256Hash == "" {
		dbModel.SHA256Hash = version.Data.SHA256Hash
	}
	if dbModel.Size == 0 {
		dbModel.Size = version.Data.Size
	}
	tags := make([]model.Tag, 0)
	if version.Data.Tags != nil {
		for _, tag := range version.Data.Tags {
			tags = append(tags, model.Tag{Name: tag})
		}
	}
	dbModel.Tags = tags
	return nil
}

// Verify - Representation impl
func (version *VersionMsg) Verify() error {
	if version.Data.SHA256Hash == "" {
		return errors.New("SHA256 sum is a required param")
	}
	if version.Data.Size == 0 {
		return errors.New("Size is a required param")
	}
	if version.Data.FileName == "" {
		return errors.New("file_name is a required param")
	}
	return nil
}

//FromModel -
func (versions *VersionCollection) FromModel(target interface{}) error {
	versions.Version = versionVersion
	versions.Type = versionCollectionType
	models, ok := target.(*model.Versions)
	log.Print(target)
	log.Print("type:")
	log.Print(reflect.TypeOf(target).Name())
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(target).Name())
	}
	versions.Data = make([]Version, 0, len(*models))
	for _, model := range *models {
		var versionMsg VersionMsg
		err := versionMsg.FromModel(&model)
		if err != nil {
			return err
		}
		versions.Data = append(versions.Data, versionMsg.Data)
	}
	versions.Count = len(versions.Data)
	return nil
}

// ToModel -
func (versions *VersionCollection) ToModel(target interface{}) error {
	return nil
}

// Verify -
func (versions *VersionCollection) Verify() error {
	return nil
}
