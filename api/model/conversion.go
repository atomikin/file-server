package model

import (
	"fmt"
	"log"
	"math"
	"reflect"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

const (
	StatusCreated    = "created"
	StatsuInProgress = "in_progress"
	StatusSuccess    = "success"
	StatusError      = "error"
)

// Conversion -
type Conversion struct {
	Base
	SrcVersionID    uuid.UUID `gorm:"type:uuid;not null;unique_index:version_preset_index"`
	Progress        float32   `gorm:"default:0;"`
	ResultVersionID uuid.UUID `gorm:"type:uuid;index;"`
	SrcVersion      Version   `gorm:"foreignkey:SrcVersionID"`
	ResultVersion   Version   `gorm:"foreignkey:ResultVersionID;"`
	NewName         string    `gorm:"index"`
	Preset          string    `gorm:"unique_index:version_preset_index"`
	Status          string    `gorm:"default:'created'"`
}

// Conversions -
type Conversions []Conversion

// ConversionRepository -
type ConversionRepository struct {
	db *gorm.DB
}

//Save -
func (repo *ConversionRepository) Save(val interface{}) error {
	conv, ok := val.(*Conversion)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(val).Name())
	}
	if repo.db.NewRecord(conv) {
		if err := repo.db.Create(conv).Error; err != nil {
			return err
		}
	} else {
		if err := repo.db.Save(conv).Error; err != nil {
			return err
		}
	}
	return nil
}

// Delete -
func (repo *ConversionRepository) Delete(obj interface{}) error {
	if conv, ok := obj.(*Conversion); ok {
		return repo.db.Delete(conv).Error
	}
	return fmt.Errorf("Incorrect type %s", reflect.TypeOf(obj).Name())
}

// FindByID -
func (repo *ConversionRepository) FindByID(id interface{}, target interface{}) error {
	return repo.db.Preload("SrcVersion").Where("id = ?", id).First(target).Error
}

// FindBy -
func (repo *ConversionRepository) FindBy(obj interface{}, pairs ...Arg) error {
	convs, ok := obj.(*Conversions)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(obj).Name())
	}
	searchableFields := map[string]bool{
		"src_file_id":       true,
		"src_version_id":    true,
		"result_version_id": true,
		"progress":          true,
		"created_at":        true,
		"updated_at":        true,
		"status":            true,
	}
	db := repo.db
	for _, arg := range pairs {
		log.Print(arg)
		if _, ok := searchableFields[arg.Field]; !ok {
			continue
		}
		db = addSelectBy(db, arg)
	}
	return db.Preload("SrcVersion").Preload("ResultVersion").Find(convs).Error
}

// Exists -
func (repo *ConversionRepository) Exists(obj interface{}) bool {
	if conv, ok := obj.(*Conversion); ok {
		return !repo.db.NewRecord(conv)
	}
	return false
}

// Slice - Slice interface impl
func (convs *Conversions) Slice(from, to int) {
	max := len(*convs)
	from = int(math.Min(float64(from), float64(max)))
	to = int(math.Min(float64(to), float64(max)))
	log.Printf("Slicing from %d to %d", from, to)
	*convs = (*convs)[from:to]
}

// Len - Slice interface impl
func (convs *Conversions) Len() int {
	return len(*convs)
}
