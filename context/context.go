package context

import (
	"log"

	"github.com/jinzhu/gorm"
	"gitlab.com/atomikin/file-server/api/converter"
	"gitlab.com/atomikin/file-server/common/config"
	"gitlab.com/atomikin/file-server/storage"
)

// Context acts as provider of shared resources, DB conns, caches and so on
type Context interface {
	DB() *gorm.DB
	Clone() Context
	Cfg() *config.APIConf
	Storage() storage.FileStorage
	Converter() converter.Converter
}

type AppContext struct {
	database      *gorm.DB
	configuration *config.APIConf
	dbConfig      *config.DBConf
	storage       storage.FileStorage
	converter     converter.Converter
}

// DB returns an active DB connection
func (context *AppContext) DB() *gorm.DB {
	err := context.database.DB().Ping()
	if err != nil {
		db, err := openDBConn(context.dbConfig)
		if err != nil {
			log.Fatalf("fatal error: %s", err.Error())
		}
		context.database = db
	}
	return context.database
}

// Clone - copy context before transferring to
func (context *AppContext) Clone() Context {
	ctx := *context
	ctx.database = context.database.New()
	return &ctx
}

// Cfg - API part of the config
func (context *AppContext) Cfg() *config.APIConf {
	return context.configuration
}

// Storage -
func (context *AppContext) Storage() storage.FileStorage {
	return context.storage
}

//Converter - getter for converter
func (context *AppContext) Converter() converter.Converter {
	return context.converter
}

func openDBConn(cfg *config.DBConf) (*gorm.DB, error) {
	return gorm.Open(cfg.Dialect, cfg.URI)
}

// New creates a new context with all the connections
func New(cfg *config.Configuraton) Context {
	db, err := openDBConn(&cfg.DB)
	if err != nil {
		log.Fatal(err.Error())
	}
	db.LogMode(cfg.DB.DebugLog)
	storage, err := storage.NewFSStorage(&cfg.Storage)
	if err != nil {
		log.Fatal(err)
	}
	return &AppContext{
		database:      db,
		configuration: &cfg.API,
		storage:       storage,
		dbConfig:      &cfg.DB,
		converter:     converter.NewConverter(&cfg.Converter),
	}
}
