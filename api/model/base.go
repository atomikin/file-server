package model

import (
	"fmt"
	"log"
	"math"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

const (
	OpGt   = ">"
	OpLt   = "<"
	OpLe   = "<="
	OpGe   = ">="
	OpIn   = "in"
	OpEq   = "="
	OpLike = "LIKE"
)

// Base contains common columns for all tables.
type Base struct {
	ID        uuid.UUID  `gorm:"type:uuid;primary_key;" json:"id"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `sql:"index" json:"deleted_at"`
}

// Slice -
type Slice interface {
	Slice(int, int)
	Len() int
}

// BeforeCreate will set a UUID rather than numeric ID.
func (base *Base) BeforeCreate(scope *gorm.Scope) error {
	uuid.NewV4()
	uuid := uuid.NewV4()
	return scope.SetColumn("ID", uuid)
}

// ParseLimitOffset - limit, offset
func ParseLimitOffset(page, size string) *Pagination {
	var intPage, intSize int
	var err error
	if page == "" {
		intPage = 0
	} else {
		intPage, err = strconv.Atoi(page)
		if err != nil {
			intPage = 0
		} else if intPage != 0 {
			intPage = int(math.Abs(float64(intPage))) - 1
		}
	}
	if size == "" {
		return &Pagination{Offset: 0, Limit: 0}
	}
	intSize, err = strconv.Atoi(size)
	if err != nil {
		return &Pagination{Offset: 0, Limit: 0}
	}
	intSize = int(math.Abs(float64(intSize)))
	return &Pagination{Limit: intSize, Offset: intSize * intPage}
}

// Pagination -
type Pagination struct {
	Offset int
	Limit  int
}

func (page *Pagination) String() string {
	q := make([]string, 0, 2)
	if page.Offset != 0 {
		q = append(q, fmt.Sprintf("offset %d rows", page.Offset))
	}
	if page.Limit != 0 {
		q = append(q, fmt.Sprintf("limit %d", page.Limit))
	}
	return strings.Join(q, " ")
}

// Paginate collection
func (page *Pagination) Paginate(slice Slice) {
	total := slice.Len()
	if total <= page.Offset {
		slice.Slice(0, 0)
	} else if page.Limit > 0 {
		last := total
		if page.Offset+page.Limit-1 < last {
			last = page.Offset + page.Limit
		}
		log.Printf("Files from %d to %d", page.Offset, last)
		slice.Slice(page.Offset, last)
	}
}

// Arg -
type Arg struct {
	Field string
	Value interface{}
	Op    string // in, >, <, = (default)
}

// FieldOp -
func (arg Arg) FieldOp() string {
	switch arg.Op {
	case OpGt, OpGe, OpLe, OpLt, OpEq, OpLike:
		return fmt.Sprintf("%s %s ?", arg.Field, arg.Op)
	case OpIn:
		return fmt.Sprintf("%s %s (?)", arg.Field, arg.Op)
	default:
		return fmt.Sprintf(`%s = ?`, arg.Field)
	}
}

// Repository -
type Repository interface {
	Save(interface{}) error
	Delete(interface{}) error
	FindByID(interface{}, interface{}) error
	FindBy(interface{}, ...Arg) error
	Exists(interface{}) bool
}

func addSelectBy(tx *gorm.DB, arg Arg) *gorm.DB {
	log.Print(arg.FieldOp())
	return tx.Where(arg.FieldOp(), arg.Value)
	// return tx
}

func addSelectByTag(tx *gorm.DB, arg Arg, baseTable, joinTable, joinField string) *gorm.DB {
	tags, ok := arg.Value.([]string)
	if !ok || len(tags) == 0 {
		return tx
	}
	lenTags := len(tags)
	return tx.Joins(fmt.Sprintf("cross join %s", joinTable)).
		Select(fmt.Sprintf("%s.*, count(*) cnt", baseTable)).
		Where(fmt.Sprintf("%s.tag_name in (?)", joinTable), tags).
		Where(fmt.Sprintf("%s.%s = %s.id", joinTable, joinField, baseTable)).
		Group(fmt.Sprintf("%s.id", baseTable)).
		Having("count(*) = ?", lenTags)
}

// NewRepo -
func NewRepo(db *gorm.DB, obj interface{}) (Repository, error) {
	switch obj.(type) {
	case *File:
		r := &FileRepository{db: db}
		return r, nil
	case *Version:
		r := &VersionRepository{db: db}
		return r, nil
	case *Conversion:
		r := &ConversionRepository{db: db}
		return r, nil
	default:
		return nil, fmt.Errorf("Incorrect type %s", reflect.TypeOf(obj).Name())
	}
}
