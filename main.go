package main

import (
	"os"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/atomikin/file-server/common/config"
	"gitlab.com/atomikin/file-server/daemon"
)

func main() {
	config := config.Load(os.Args[1])
	//utils.InitLogger(config)
	daemon.Run(config)
}
