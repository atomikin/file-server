package controller

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/atomikin/file-server/api/model"
	repr "gitlab.com/atomikin/file-server/api/representation"
	"gitlab.com/atomikin/file-server/context"
)

// PostFileHandler creates files
func PostFileHandler(ctx context.Context, w http.ResponseWriter, req *http.Request) {
	file := new(repr.FileMsg)
	err := repr.Load(file, req.Body)
	if err != nil {
		WriteResp(w, http.StatusBadRequest, repr.NewErrorResp(err))
		return
	}
	if err = file.Verify(); err != nil {
		WriteResp(w, http.StatusBadRequest, repr.NewErrorResp(err))
		return
	}
	// Obliterate generated data
	file.Data.ID = uuid.UUID{}
	file.Data.CreatedAt = time.Time{}
	var dbFile model.File
	err = file.ToModel(&dbFile)
	if err != nil {
		WriteResp(w, http.StatusBadRequest, repr.NewErrorResp(err))
		return
	}
	tx := ctx.DB().Begin()
	repo, _ := model.NewRepo(tx, &model.File{})
	// defer func() {
	// 	if err != nil {
	// 		tx.Rollback()
	// 		log.Print("Rolledback transaction")
	// 		return
	// 	}
	// }()
	err = repo.Save(&dbFile)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusBadRequest, repr.NewErrorResp(err))
		return
	}
	resp := new(repr.FileMsg)
	err = resp.FromModel(&dbFile)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusBadRequest, repr.NewErrorResp(err))
		return
	}
	err = tx.Commit().Error
	if err != nil {
		WriteResp(w, http.StatusBadRequest, repr.NewErrorResp(err))
		return
	}
	log.Print("Committed transaction")
	WriteResp(w, http.StatusCreated, resp)
}

// GetFileHandler - get saved file
func GetFileHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	log.Printf("Got get file request ID %s", vars["file_id"])
	var file model.File
	repo, _ := model.NewRepo(ctx.DB(), &file)
	err := repo.FindByID(vars["file_id"], &file)
	if err != nil {
		err := repr.NewErrorResp(
			fmt.Errorf("File not found for id %s", vars["file_id"]),
		)
		WriteResp(w, http.StatusNotFound, err)
		return
	}
	log.Println(file.Tags)
	rep := new(repr.FileMsg)
	err = rep.FromModel(&file)
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	WriteResp(w, http.StatusOK, rep)
}

// PutFileHandler -
func PutFileHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	input := new(repr.FileMsg)
	var err error
	if err = repr.Load(input, r.Body); err != nil {
		WriteResp(w, http.StatusBadRequest, repr.NewErrorResp(err))
		return
	}
	if err = input.Verify(); err != nil {
		WriteResp(w, http.StatusBadRequest, repr.NewErrorResp(err))
		return
	}
	tx := ctx.DB().Begin()
	// defer func() {
	// 	if err != nil {
	// 		tx.Rollback()
	// 		log.Print("Rolledback transaction")
	// 	}
	// }()
	file := new(model.File)
	repo, err := model.NewRepo(tx, file)
	if err = repo.FindByID(vars["file_id"], file); err != nil {
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		return
	}
	if err = input.ToModel(file); err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	if err = repo.Save(file); err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	output := new(repr.FileMsg)
	if err = output.FromModel(file); err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	err = tx.Commit().Error
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	WriteResp(w, http.StatusOK, output)
}

// SearchFilesHandler -
func SearchFilesHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	files := make(model.Files, 0)
	query := r.URL.Query()
	page := model.ParseLimitOffset(query.Get("page"), query.Get("size"))
	repo, _ := model.NewRepo(ctx.DB(), &model.File{})
	args := make([]model.Arg, 0)
	for _, name := range []string{"name", "folder"} {
		value := query.Get(name)
		if value != "" {
			args = append(args, model.Arg{Field: name, Value: value, Op: model.OpLike})
		}
	}
	args = append(args, model.Arg{Field: "tags", Value: query["tags"]})
	err := repo.FindBy(&files, args...)
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	totalFiles := len(files)

	f := model.Files(files)
	page.Paginate(&f)
	fileColl := new(repr.FileCollection)
	err = fileColl.FromModel(f)

	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	fileColl.Total = totalFiles
	fileColl.Offset = page.Offset
	WriteResp(w, http.StatusOK, fileColl)
}

// DeleteFileHandler -
func DeleteFileHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fileID := vars["file_id"]
	tx := ctx.DB().Begin()
	file := new(model.File)
	repo, _ := model.NewRepo(tx, file)
	err := repo.FindByID(fileID, file)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	err = repo.Delete(file)
	resp := new(repr.FileMsg)
	err = resp.FromModel(file)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	tx.Commit()
	WriteResp(w, http.StatusOK, resp)
}
