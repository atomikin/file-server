FROM debian:buster-slim

RUN mkdir /storage
RUN mkdir /config

COPY ./docker_config.yml /config/config.yml
COPY ./build/file-server /usr/bin/file-server

VOLUME [ "/storage" ]

EXPOSE 8080/tcp

CMD [ "/usr/bin/file-server", "/config/config.yml" ]