package controller

import (
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/atomikin/file-server/api/model"
	repr "gitlab.com/atomikin/file-server/api/representation"
	"gitlab.com/atomikin/file-server/context"
)

// PatchConvertion -
func PatchConvertion(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	conv := new(model.Conversion)
	tx := ctx.DB().Begin()
	repo, err := model.NewRepo(tx, conv)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	err = repo.FindByID(vars["conv_id"], conv)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		return
	}
	reprFromClient := new(repr.ConversionMsg)
	err = repr.Load(reprFromClient, r.Body)
	if err == nil {
		err = reprFromClient.Verify()
	}
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		return
	}
	err = reprFromClient.ToModel(conv)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	err = repo.Save(conv)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	err = tx.Commit().Error
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	reprFromClient.FromModel(conv)
	WriteResp(w, http.StatusOK, reprFromClient)
}

// GetConversion -
func GetConversion(ctx context.Context, w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	conversion := new(model.Conversion)
	repo, _ := model.NewRepo(ctx.DB(), conversion)
	err := repo.FindByID(vars["conv_id"], conversion)
	if err != nil {
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		return
	}
	rep := new(repr.ConversionMsg)
	err = rep.FromModel(conversion)
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	WriteResp(w, http.StatusOK, rep)
}

// GetConversions - search conversions by criteria
func GetConversions(ctx context.Context, w http.ResponseWriter, req *http.Request) {
	// vars := mux.Vars(req)
	query := req.URL.Query()
	args := make([]model.Arg, 0)
	for field := range query {
		log.Printf("ARG: %s = %s", field, query.Get(field))
		switch field {
		case "updated_after":
			args = append(args, model.Arg{Field: "updated_at", Value: query.Get(field), Op: model.OpGe})
		case "completed":
			value := strings.ToLower(query.Get(field))
			var operation string
			if value == "true" {
				operation = model.OpGe
			} else if value == "false" {
				operation = model.OpLt
			} else {
				continue
			}
			args = append(
				args,
				model.Arg{
					Field: "progress",
					Value: 100,
					Op:    operation,
				},
			)
		default:
			args = append(args, model.Arg{Field: field, Value: query.Get, Op: model.OpEq})
		}
	}
	var conversions model.Conversions
	rep := new(repr.ConversionsMsg)
	repo, err := model.NewRepo(ctx.DB(), &model.Conversion{})
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	err = repo.FindBy(&conversions, args...)
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	total := len(conversions)
	page := model.ParseLimitOffset(query.Get("page"), query.Get("size"))
	page.Paginate(&conversions)
	err = rep.FromModel(&conversions)
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	rep.Total = total
	rep.Offset = page.Offset
	WriteResp(w, http.StatusOK, rep)
}

// RestartConversion - Restart incomplete conversion
func RestartConversion(ctx context.Context, w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	conv := new(model.Conversion)
	tx := ctx.DB().Begin()
	repo, _ := model.NewRepo(ctx.DB(), conv)
	err := repo.FindByID(vars["conv_id"], conv)
	if err != nil {
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		tx.Rollback()
		return
	}
	rep := new(repr.ConversionMsg)
	if conv.Status == model.StatusSuccess {
		defer tx.Rollback()
		err := rep.FromModel(conv)
		if err != nil {
			WriteResp(w, http.StatusBadRequest, repr.NewErrorResp(err))
			return
		}
		WriteResp(w, http.StatusOK, rep)
		return
	}
	conv.Status = model.StatusCreated
	err = repo.Save(conv)
	if err != nil {
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		tx.Rollback()
		return
	}
	err = rep.FromModel(conv)
	if err != nil {
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		tx.Rollback()
		return
	}
	err = ctx.Converter().Restart(conv)
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		tx.Rollback()
		return
	}
	tx.Commit()
	WriteResp(w, http.StatusCreated, rep)
}
