package representation

import (
	"errors"
	"fmt"
	"reflect"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/atomikin/file-server/api/model"
)

const (
	convVersion = 1
	convType    = "conv"
	convsType   = "convs"
)

// Conversion -
type Conversion struct {
	ID            uuid.UUID `json:"id"`
	SrcVersion    Version   `json:"src_version"`
	ResultVersion uuid.UUID `json:"result_version"`
	Progress      float32   `json:"progress"`
	NewName       string    `json:"new_name"`
	Preset        string    `json:"preset"`
	UpdatedAt     time.Time `json:"updated_at"`
	Status        string    `json:"status"`
}

// Conversions -
type Conversions []Conversion

//ConversionMsg -
type ConversionMsg struct {
	BaseMsg
	Data Conversion `json:"data"`
}

// ConversionsMsg -
type ConversionsMsg struct {
	BaseCollectionMsg
	Data Conversions `json:"data"`
}

// FromModel -
func (cv *ConversionMsg) FromModel(val interface{}) error {
	convMod, ok := val.(*model.Conversion)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(val).Name())
	}
	versionRep := new(VersionMsg)
	err := versionRep.FromModel(&convMod.SrcVersion)
	if err != nil {
		return err
	}
	cv.Type = convType
	cv.Version = convVersion
	cv.Data.NewName = convMod.NewName
	cv.Data.ID = convMod.ID
	cv.Data.SrcVersion = versionRep.Data
	cv.Data.Preset = convMod.Preset
	cv.Data.Progress = convMod.Progress
	cv.Data.UpdatedAt = convMod.UpdatedAt
	cv.Data.Status = convMod.Status
	cv.Data.ResultVersion = convMod.ResultVersionID
	return nil
}

// ToModel - interface impl
func (cv *ConversionMsg) ToModel(val interface{}) error {
	convMod, ok := val.(*model.Conversion)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(val).Name())
	}
	nullID := uuid.UUID{}
	if convMod.ID == nullID {
		convMod.ID = cv.Data.ID
	}
	if convMod.SrcVersionID == nullID {
		convMod.SrcVersionID = cv.Data.SrcVersion.ID
	}
	if cv.Data.Status != "" {
		convMod.Status = cv.Data.Status
	}
	if cv.Data.ResultVersion != nullID {
		convMod.ResultVersionID = cv.Data.ResultVersion
	}
	convMod.Progress = cv.Data.Progress
	return nil
}

// Verify - inteface impl
func (cv *ConversionMsg) Verify() error {
	if cv.Data.Progress <= 0 {
		return errors.New("Progress is expected to be greater or equal to 0")
	}
	return nil
}

// FromModel -
func (convs *ConversionsMsg) FromModel(val interface{}) error {
	convsMod, ok := val.(*model.Conversions)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(val).Name())
	}
	data := make(Conversions, 0)
	for _, convModel := range *convsMod {
		representation := new(ConversionMsg)
		err := representation.FromModel(&convModel)
		if err != nil {
			return nil
		}
		data = append(data, representation.Data)
	}
	convs.Count = len(*convsMod)
	convs.Data = data
	return nil
}

// ToModel -
func (convs *ConversionsMsg) ToModel(val interface{}) error {
	convsMod, ok := val.(*model.Conversions)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(val).Name())
	}
	result := make(model.Conversions, convs.Count)
	for _, conv := range convs.Data {
		dbObj := model.Conversion{
			SrcVersionID: conv.SrcVersion.ID,
			Progress:     conv.Progress,
			Preset:       conv.Preset,
			NewName:      conv.NewName,
		}
		result = append(result, dbObj)
	}
	*convsMod = result
	return nil
}

// Verify -
func (convs *ConversionsMsg) Verify() error {
	for _, conv := range convs.Data {
		if conv.Progress <= 0 {
			return errors.New("Progress is expected to be greater than 0")
		}
		if conv.Status == "" {
			return errors.New("Status is required")
		}
	}
	return nil
}
