package representation

const (
	errorType    = "error"
	errorVersion = 1
)

// Error - error response
type Error struct {
	Data    string `json:"data"`
	Version int    `json:"version"`
	Type    string `json:"type"`
}

// NewErrorResp - create error resp from error obj
func NewErrorResp(err error) *Error {
	return &Error{Data: err.Error(), Type: errorType, Version: errorVersion}
}

// NewError Create new error message from string
func NewError(err string) *Error {
	return &Error{Data: err, Type: errorType, Version: errorVersion}
}

// Formal Representaion interface implementation

// FromModel -
func (err *Error) FromModel(target interface{}) error {
	return nil
}

// ToModel -
func (err *Error) ToModel(target interface{}) error {
	return nil
}

// Verify -
func (err *Error) Verify() error {
	return nil
}

// // Bytes - marshall Error
// func (err *Error) Bytes() []byte {
// 	data, _ := json.Marshal(err)
// 	return data
// }
