package utils

import (
	"log"
	"log/syslog"

	"gitlab.com/atomikin/file-server/common/config"
)

const (
	// LogTypeSyslog - syslog log type
	LogTypeSyslog = "syslog"
	// LogTypeConsole - log to console
	LogTypeConsole = "console"
)

// InitLogger initializes logger
func InitLogger(cfg *config.Configuraton) {
	logwriter, e := syslog.New(syslog.LOG_NOTICE, "media-server")
	if e == nil {
		log.SetOutput(logwriter)
	}
}
