package daemon

import (
	"fmt"
	"log"
	"os"
	"os/signal"

	"gitlab.com/atomikin/file-server/api"
	"gitlab.com/atomikin/file-server/common/config"
	"gitlab.com/atomikin/file-server/context"
)

/*
Run start the sever daemon
*/
func Run(cfg *config.Configuraton) {
	//utils.InitLogger(cfg)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			fmt.Fprintln(os.Stderr, "Got SigInt, gracefully exiting...")
			os.Exit(1)
		}
	}()
	ctx := context.New(cfg)
	log.Printf(
		"Started media file server on http://%s:%s ...",
		cfg.API.Host, cfg.API.Port,
	)
	api.Run(ctx)
}
