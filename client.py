#!/usr/bin/env python

import argparse
import enum
import hashlib
import pathlib
import json
import os
import shutil
import sys
import dataclasses
from typing import Dict, Any, Callable, List, Set, Generator, Sequence

import requests


class Colour(str, enum.Enum):
    RED = '\u001b[31m'
    BLACK = '\u001b[30m'
    GREEN = '\u001b[32m'
    YELLOW = '\u001b[33m'
    BLUE = '\u001b[34m'
    WHITE = '\u001b[37m'
    NC = '\u001b[0m'

    def wrap(self, line: str) -> str:
        return f'{self.value}{line}{Colour.NC}'

    def __call__(self, line: str) -> str:
        return self.wrap(line)


class TableMixin():

    def _get_col_length(self, data, max_len):
        fields = None
        for datum in data:
            if not fields:
                fields = {k: len(datum[k]) for k in sorted(datum)}
                continue
            if set(datum) != set(fields):
                raise TypeError('Mixed type of data')
            for key, value in datum.items():
                value = value if isinstance(value, str) else repr(value)
                fields[key] = min(max(fields[key], len(value)), max_len)
        # print(fields)
        return fields

    @staticmethod
    def _format_value(val: str, length: int) -> str:
        val = val if isinstance(val, str) else repr(val)
        val_len = len(val)
        dots = '...'
        if val_len > length:
            val = dots + val[val_len - length + len(dots):]
        return val

    def _format_row(self, template, field_lengths, row) -> str:
        values = {}
        for f, l in field_lengths.items():
            values[f] = self._format_value(row[f], l)
        return template.format(**values)

    def get_view(self, max_column_len=20, sep='|', 
            headers=True, borders=True, include: Set[str] = frozenset()) -> str:
        if not self.data:
            return Colour.RED('No Data\n')
        data = [dataclasses.asdict(i) for i in self.data]
        if headers:
            data = [{k: k for k in data[0]}] + data
        if include:
            data = [{k: v for k, v in i.items() if k in include} for i in data]
        col_len = self._get_col_length(data, max_column_len)
        template = f' {sep} '.join([f'{{{k}:>{v}}}' for k, v in col_len.items()])
        if borders:
            template = f'{sep} {template} {sep}'
        rows = []
        for datum in data:
            rows.append(
                self._format_row(template, col_len, datum)
            )
        line_sep = '\n'
        border = ''
        if borders:
            total_len = len(rows[0]) - 2
            fill = "+".join(["-" * (v + 2) for v in col_len.values()])
            line_sep = f'\n{sep}{fill}{sep}\n'
            border = f'\n+{"-" * total_len}+\n'
        body = f'{line_sep.join(rows)}'
        return f'{border}{body}{border}\n{"="*20}\n{self.count}/{self.total}; offset: {self.offset}'


@dataclasses.dataclass
class File:
    """
    type File struct {
        ID        uuid.UUID `json:"id"`
        Name      string    `json:"name"`
        Folder    string    `json:"folder"`
        Tags      []string  `json:"tags"`
        CreatedAt time.Time `json:"created_at"`
    }
    """
    id: str
    name: str
    folder: str
    created_at: str
    tags: List[str]


@dataclasses.dataclass
class FilesMsg(TableMixin):
    type: str
    version: int
    offset: int
    total: int
    count: int
    data: List[File]

    def __post_init__(self):
        res = []
        for i in self.data:
            res.append(File(**i))
        self.data = res


@dataclasses.dataclass
class FileMsg:
    type: str
    version: int
    data: File

    def __post_init__(self):
        self.data = File(**self.data)


@dataclasses.dataclass
class Version:
    """
    type Version struct {
        ID        uuid.UUID `json:"id"`
        CreatedAt time.Time `json:"created_at"`
        FileName  string    `json:"file_name"`
        // StorageID    uuid.UUID `json:`
        RelFile      uuid.UUID `json:"file_id"`
        SHA256Hash   string    `json:"sha256"`
        Size         uint64    `json:"size"`
        UploadedSize uint64    `json:"uploaded_size"`
        Verified     bool      `json:"verified"`
        Tags         []string  `json:"tags"`
    }
    """
    id: str
    created_at: str
    file_name: str
    file_id: str
    sha256: str
    size: int
    uploaded_size: int
    verified: bool
    tags: List[str]


@dataclasses.dataclass
class VersionsMsg(TableMixin):
    type: str
    version: int
    offset: int
    total: int
    count: int
    data: List[Version]

    def __post_init__(self):
        res = []
        for i in self.data:
            res.append(Version(**i))
        self.data = res


@dataclasses.dataclass
class VersionMsg:
    type: str
    version: int
    data: Version

    def __post_init__(self):
        self.data = Version(**self.data)


@dataclasses.dataclass
class Conversion:
    """
    type Conversion struct {
        ID uuid.UUID `json:"id"`
        // SrcURL     string    `json:"src"`
        SrcVersion Version `json:"src_version"`
        Progress   float32 `json:"progress"`
        NewName    string  `json:"new_name"`
        Preset     string  `json:"preset"`
    }
    """
    id: str
    src_version: str
    progress: float
    new_name: str
    preset: str
    updated_at: str
    status: str
    result_version: str


@dataclasses.dataclass
class ConversionMsg:
    type: str
    version: int
    data: Conversion

    def __post_init__(self):
        self.data = Conversion(**self.data)


@dataclasses.dataclass
class ConversionsMsg(TableMixin):
    type: str
    version: int
    offset: int
    total: int
    count: int
    data: List[Conversion]

    def __post_init__(self):
        self.data = [Conversion(**i) for i in self.data]


class ProgressBar:

    class ReadWriterWrapper:

        CHUNK_SIZE = 1024 * 2

        def __init__(self, file_obj, size: int, handler: Callable[[float], None]):
            self._file_obj = file_obj
            self._handler = handler
            self._size = float(size)
            self._total = 0

        def read(self, n=None) -> bytes:
            # print('read')
            data = self._file_obj.read(n)
            self._total += len(data)
            self._handler(self._total/self._size * 100)
            return data

        def write(self, data: bytes):
            self._total += len(data)
            self._handler(self._total / self._size * 100)
            return self._file_obj.write(data)

        def __iter__(self):
            while True:
                data = self._file_obj.read(self.CHUNK_SIZE)
                if not data:
                    break
                self._total += len(data)
                self._handler(self._total/self._size * 100)
                # print(data, total)
                yield data

        def __len__(self):
            return int(self._size)

        # def __getattr__(self, attr):
        #     return getattr(self._file_obj, attr)

    def __init__(self, prefix, width=100, filler='#'):

        self._prefix = Colour.YELLOW.wrap(prefix)
        self._progress = None
        self._width = width
        self._filler = filler

    def init(self):
        line = (
            f'{self._prefix}: '
            f'{Colour.GREEN.wrap("[")}'
            f'{" " * self._width}'
            f'{Colour.GREEN.wrap("]")}'
        )
        self._progress = 0
        sys.stdout.write(line)
        sys.stdout.flush()
        sys.stdout.write('\b' * (self._width + 1))
        sys.stdout.flush()

    def update(self, value: float):
        if self._progress is None:
            self.init()
        current = int(self._width * int(value) / 100)
        if current != self._progress:
            sys.stdout.write(
                Colour.GREEN.wrap(
                    self._filler * (current - self._progress)
                )
            )
            self._progress = current
            sys.stdout.flush()

    def wrap(self, file_obj, size):
        return self.ReadWriterWrapper(file_obj, size, self.update)


class MsgType(str, enum.Enum):
    VERSION = 'version'
    FILE = 'file'

class Client:

    _BASE = {
        'version': 1,
        'type': None,
        'data': {},
    }

    def __init__(self, base_url='http://127.0.0.1:8080'):
        self.url = base_url
        self._session = requests.Session()

    def files_url(self):
        return f'{self.url}/files'
    
    def file_url(self, file_id):
        if not file_id:
            raise TypeError(f'Wrong file id {file_id}')
        return f'{self.files_url()}/{file_id}'

    def versions_url(self, file_id):
        return f'{self.file_url(file_id)}/versions'

    def version_url(self, file_id, version_id):
        if not version_id:
            raise TypeError(f'Wrong version id {version_id}')
        return f'{self.versions_url(file_id)}/{version_id}'

    def content_url(self, file_id, version_id) -> str:
        return f'{self.version_url(file_id, version_id)}/content'

    def conversions_url(self):
        return f'{self.url}/conversions/'
    
    def conversion_url(self, conv_id):
        return f'{self.url}/conversions/{conv_id}'

    def get_sha256(self, filename: pathlib.Path) -> str:
        hasher = hashlib.sha256()
        with filename.open('rb') as f:
            while True:
                data = f.read(1024 * 10)
                if not len(data):
                    break
                hasher.update(data)
        return hasher.hexdigest()

    def check_resp(self, r: requests.Response):
        if r.status_code in (200, 201):
            return
        try:
            payload = r.json()
            print(
                f'Error: {payload["data"]} '
                f'(status code {r.status_code})', 
                file=sys.stderr
            )
        except Exception:
            print(f'Raw: {r.content}')
        r.raise_for_status()


    def create_file(self, filename: pathlib.Path, tags: list) -> str:
        msg = {
            'name': filename.name,
            'folder': filename.parent.name,
            'tags': tags,
        }
        m = self._BASE.copy()
        m['data'] = msg
        m['type'] = 'file'
        # print(m)
        resp = self._session.post(
            self.files_url(), data=json.dumps(m),
            headers = {'content-type': 'application/json'}
        )
        # print(resp.json())
        self.check_resp(resp)
        # print(f'status: {resp.status_code}, data: {resp.json}')
        return FileMsg(**resp.json())

    def delete_file(self, file_id=None, params: Dict[str, str] = None):
        if file_id:
            url = self.file_url(file_id)
            params = {}
        else:
            url = self.files_url()
        resp = self._session.delete(url, params=params)
        self.check_resp(resp)
        return FileMsg(**resp.json())

    def create_version(self, file_id: str, filepath: pathlib.Path, tags: list) -> dict:
        stat = filepath.stat()
        msg = dict(
            sha256=self.get_sha256(filepath),
            file_name=filepath.name,
            size=stat.st_size,
            tags=tags,
        )
        m = self._BASE.copy()
        m['type'] = 'version'
        m['data'] = msg
        # print('data to send: ', m)
        resp = self._session.post(
            self.versions_url(file_id), 
            data=json.dumps(m),
            headers={'content-type': 'application/json'},
        )
        # print('version: ', resp.json())
        self.check_resp(resp)
        # print(f'status: {resp.status_code}, data: {resp.json()}')
        return VersionMsg(**resp.json())

    def upload_version_content(self, file_obj, file_id: str, version_id: str):
        # version = version_json['data']
        # with filepath.open('rb') as f:
        version = self.get_version(file_id, version_id)
        if version.data.uploaded_size:
            file_obj.read(version.data.uploaded_size)
        resp = self._session.post(
            self.content_url(version.data.file_id, version.data.id),
            headers = {'content-type': 'binary/octet-stream', 'x-offset': '0'},
            data=file_obj
        )
        # print(resp.content)
        resp.raise_for_status()
        # print(f'status: {resp.status_code}, data: {resp.content}')

    def get_version(self, file_id: str, version_id: str) -> VersionMsg:
        resp = self._session.get(self.version_url(file_id, version_id))
        self.check_resp(resp)
        # print(f'status: {resp.status_code}, data: {resp.content}')
        return VersionMsg(**resp.json())

    def get_file(self, file_id: str, params: Dict[str, Any] = None):
        resp = self._session.get(
            self.file_url(file_id),
            headers={'content-type': 'application/json'},
            params=params
        )
        self.check_resp(resp)
        # print('file: ', resp.json())
        return FileMsg(**resp.json())
    
    def get_file_versions(self, file_id: str, params: Dict[str, Any] = None) -> VersionsMsg:
        if not file_id:
            raise TypeError("File ID is required")
        resp = self._session.get(
            self.versions_url(file_id),
            params=params,
        )
        self.check_resp(resp)
        return VersionsMsg(**resp.json())

    def get_files(self, params: Dict[str, Any]) -> FilesMsg:
        resp = self._session.get(
            self.files_url(), params=params
        )
        self.check_resp(resp)
        return FilesMsg(**resp.json())

    def download_file(self, file_id: str, version_id: str, store_path: pathlib.Path):
        file = self.get_file(file_id)
        version = self.get_version(file_id, version_id)
        path = store_path.joinpath(file.data.folder)
        path.mkdir(parents=True, exist_ok=True)
        path = path.joinpath(version.data.file_name)
        with self._session.get(self.content_url(file_id, version_id), stream=True) as r:
            pb = ProgressBar(str(version.data.file_name), width=30)
            with path.open('wb') as f:
                shutil.copyfileobj(r.raw, pb.wrap(f, version.data.size))
        hs = self.get_sha256(path)
        if hs != version.data.sha256:
            raise RuntimeError(
                f"Hashes doen't match: {hs} != {version.data.sha256}"
            )
        return path

    def get_conversions(self, params) -> ConversionsMsg:
        resp = self._session.get(self.conversions_url(), params=params)
        self.check_resp(resp)
        return ConversionsMsg(**resp.json())


def get_file_list(dir_path: pathlib.Path) -> Generator[pathlib.Path, None, None]:
    for root, _, files in os.walk(dir_path):
        for file in files:
            yield pathlib.Path(root).joinpath(file)


def expand_list(*files: pathlib.Path) -> Generator[pathlib.Path, None, None]:
    for entry in files:
        if entry.is_file():
            yield entry
        elif entry.is_dir():
            for file in get_file_list(entry):
                yield file
        else:
            print(Colour.RED(f'Wrong FS entry: {entry}'))


def save_file(args: argparse.Namespace):#, filepath: pathlib.Path, file_id: str, tags: list):
    client = Client(args.url)
    file_id = args.file_id
    files_list = sorted(expand_list(*args.files))
    file_id = args.file_id if len(files_list) == 1 else None
    for file in files_list:
        try:
            try:
                file_id = file_id or client.create_file(file, args.tags).data.id
            except Exception as err:
                print(Colour.RED(f'ERROR: Cannot create file {str(file)} {str(err)}'))
                continue
            version = client.create_version(file_id, file, args.tags)
            with file.open('rb') as f:
                progress = ProgressBar(version.data.file_name, width=30)
                client.upload_version_content(progress.wrap(f, version.data.size), file_id, version.data.id)
                print('\n', f'Uploaded file {version.data.file_name}(FILE ID {version.data.file_id})')
        finally:
            file_id = None


def download_file(args: argparse.Namespace):
    client = Client(args.url)
    file_ids = []
    params = {
        k: v for k, v in 
        {'tags': args.tags, 'name': args.name, 'folder': args.folder}.items() if v
    }
    if args.file_id:
        file_ids.append(args.file_id)
    elif params:
        files = client.get_files(params)
        # print(files)
        file_ids.extend([i.id for i in files.data])
    else:
        raise TypeError("FileID or params are required")
    # print(f'File ID {file_ids}')
    for file_id in file_ids:
        # print(f'File ID {file_id}')
        versions = client.get_file_versions(file_id, dict(limit=1))
        if len(file_ids) == 1 and args.version_id:
            versions.data = [i for i in versions.data if i.id == version.id]
        if not versions.data:
            print(
                Colour.RED(f'File ID {file_id} does not have an uploaded version'),
                file=sys.stderr
            )
            sys.exit(1)

        version = versions.data[0]
    # else:
    #     version = client.get_version(args.file_id, args.version_id).data
        print(
            Colour.GREEN(f"\nDownloaded file ({file_id}), version: {version.id}({version.created_at})"),
            Colour.GREEN("\nPath: "),
            Colour.YELLOW(str(client.download_file(file_id, version.id, args.path)))
        )


def get_versions(args: argparse.Namespace):
    client = Client(args.url)
    if not args.file_id:
        raise RuntimeError('File ID are required')
    params = dict(
        limit=args.limit,
        page=args.page,
        tags=args.tags,
    )
    data = client.get_file_versions(args.file_id, params)
    print(data.get_view(max_column_len=args.table_col_length, include=args.fields))


def query_files(args: argparse.Namespace):
    client = Client(args.url)
    max_col_len = args.table_col_length
    params = vars(args)
    # print(params)
    include = params.pop('include', None)
    # print(args)
    query_params = ('folder', 'name', 'tags', 'verified', 'page', 'limit')
    for arg in params.copy():
        if arg not in query_params or params[arg] is None:
            params.pop(arg)
    print('Files matching params: ', ''.join(Colour.YELLOW(f'\n\t{k}="{v}"') for k, v in params.items()))
    print()
    files = client.get_files(params)
    # print(include)
    # print(args)
    print(files.get_view(max_column_len=max_col_len, include=set(include)))


def get_conversions(args: argparse.Namespace):
    params = {}
    if args.completed:
        params['completed'] = args.completed == 'yes'
    if args.updated_after:
        params['updated_after'] = args.updated_after
    if args.version_id:
        params['version_id'] = args.version_id
    client = Client(args.url)
    convs = client.get_conversions(params)
    # print(args)
    print('\nFiles matching params: ', ''.join(
        Colour.YELLOW(f'\n\t{k}="{v}"') for k, v in params.items()))
    params.update(
        page=args.page,
        limit=args.limit,
    )
    print(convs.get_view(max_column_len=args.table_col_length, include=args.fields))

def delete_file(args):
    file_id = args.file_id
    client = Client(args.url)
    msg = client.delete_file(file_id=file_id)
    print(f'Deleted file ID={msg.data.id}, and all it`s versions.')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--url', help='Server URL')
    parser.add_argument('-s', '--table-col-length', default=20, type=int)
    parser.add_argument('-p', '--page', type=int, default=0)
    parser.add_argument('-l', '--limit', type=int, default=0)
    subparsers = parser.add_subparsers()
    save_parser = subparsers.add_parser('save')
    download_parser = subparsers.add_parser('download')
    versions_parser = subparsers.add_parser('versions')
    conversions_parser = subparsers.add_parser('conversions')
    files_parser = subparsers.add_parser('files')
    delete_parser = subparsers.add_parser('delete')
    # parser.add_argument('command', choices=('save', 'download-version', 'versions'))

    # save
    save_parser.add_argument('-F', '--files', type=pathlib.Path, nargs='+')
    save_parser.add_argument('-t', '--tags', nargs='*')
    save_parser.add_argument('-f', '--file-id', help='file ID')
    save_parser.set_defaults(func=save_file)

    # download
    download_parser.add_argument('-v', '--version-id', help = 'Version ID')
    download_parser.add_argument('-f', '--file-id', help = 'file ID')
    download_parser.add_argument('--path', type=pathlib.Path, default=pathlib.Path.cwd)
    download_parser.add_argument('--name')
    download_parser.add_argument('--folder')
    download_parser.add_argument('--tags', nargs='*', default=())
    download_parser.set_defaults(func=download_file)

    # # download by
    # download_by

    # files
    files_parser.add_argument('-f', '--folder')
    files_parser.add_argument('-n', '--name')
    files_parser.add_argument('-t', '--tags', nargs='*')
    files_parser.add_argument('-I', '--include', nargs='*', default=set())
    files_parser.add_argument('--verified', choices=('yes', 'no'))
    files_parser.set_defaults(func=query_files)

    # versions
    versions_parser.add_argument('-f', '--file-id')
    versions_parser.add_argument('-t', '--tags', nargs='*')
    versions_parser.add_argument('-F', '--fields', nargs='*', default=())
    versions_parser.add_argument('--verified', action='store_true')
    versions_parser.set_defaults(func=get_versions)

    # conversions
    conversions_parser.add_argument('--version-id')
    conversions_parser.add_argument('--completed', choices=('yes', 'no'))
    conversions_parser.add_argument('--updated-after')
    conversions_parser.add_argument('--fields', nargs='*', default=())
    conversions_parser.set_defaults(func=get_conversions)

    # delete-files
    delete_parser.add_argument('--file-id')
    delete_parser.set_defaults(func=delete_file)


    parser.add_argument('-f', '--file-id', help='file ID')
    parser.add_argument('-V', '--version-id', help='Version ID')
    parser.add_argument('--tags', nargs='+', default=())
    args = parser.parse_args()
    # print(args)
    args.func(args)


if __name__ == '__main__':
    main()
