module gitlab.com/atomikin/file-server

go 1.13

require (
	github.com/go-redis/redis/v8 v8.0.0-beta.2
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.12
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.0
	gopkg.in/yaml.v2 v2.3.0
)
