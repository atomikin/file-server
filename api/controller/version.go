package controller

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/atomikin/file-server/api/model"
	repr "gitlab.com/atomikin/file-server/api/representation"
	"gitlab.com/atomikin/file-server/context"
)

// PostVersionHandler -
func PostVersionHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	version := new(repr.VersionMsg)
	err := repr.Load(version, r.Body)
	if err != nil {
		WriteResp(w, http.StatusBadRequest, repr.NewErrorResp(err))
		return
	}
	err = version.Verify()
	log.Print(version)
	if err != nil {
		WriteResp(w, http.StatusBadRequest, repr.NewErrorResp(err))
		return
	}
	versionModel := new(model.Version)
	vars := mux.Vars(r)
	tx := ctx.DB().Begin()
	file := new(model.File)
	repo, _ := model.NewRepo(tx, file)
	err = repo.FindByID(vars["file_id"], file)
	if err != nil {
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		return
	}
	err = version.ToModel(versionModel)
	log.Print(versionModel)
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	versionModel.RelFile = file.ID
	repo, _ = model.NewRepo(tx, versionModel)
	err = repo.Save(versionModel)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	err = version.FromModel(versionModel)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	err = tx.Commit().Error
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	WriteResp(w, http.StatusCreated, version)
}

// GetVersionHandler - get file version by id
func GetVersionHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	version := new(model.Version)
	repo, err := model.NewRepo(ctx.DB(), version)
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewError("Internal Error"))
		return
	}
	err = repo.FindByID(vars["version_id"], version)
	if err != nil {
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		return
	}
	if version.RelFile.String() != vars["file_id"] {
		WriteResp(
			w, http.StatusNotFound,
			repr.NewError(
				fmt.Sprintf("No version %s for file %s", vars["version_id"], vars["file_id"]),
			),
		)
		return
	}
	resp := new(repr.VersionMsg)
	err = resp.FromModel(version)
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	WriteResp(w, http.StatusOK, resp)
}

// GetVersionsHandler -
func GetVersionsHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	query := r.URL.Query()
	page := model.ParseLimitOffset(query.Get("page"), query.Get("size"))
	versions := make(model.Versions, 0)
	tx := ctx.DB().Begin()
	var err error
	// defer func() {
	// 	if err != nil {
	// 		tx.Rollback()
	// 	} else {
	// 		tx.Commit()
	// 	}
	// }()
	repo, err := model.NewRepo(tx, &model.Version{})
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewError("Internal error"))
		return
	}
	args := make([]model.Arg, 0)
	for _, name := range []string{"sha256_hash", "file_name", "id"} {
		if query.Get(name) != "" {
			args = append(args, model.Arg{Field: name, Value: query.Get(name)})
		}
	}
	if len(query["tags"]) != 0 {
		args = append(args, model.Arg{Field: "tags", Value: query["tags"]})
	}
	args = append(args, model.Arg{Field: "rel_file", Value: vars["file_id"]})

	err = repo.FindBy(&versions, args...)
	if err != nil {
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		return
	}
	page.Paginate(&versions)

	resp := new(repr.VersionCollection)
	err = resp.FromModel(&versions)
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	WriteResp(w, http.StatusOK, resp)
}

// PatchVersionHandler -
func PatchVersionHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	input := new(repr.VersionMsg)
	err := repr.Load(input, r.Body)
	vars := mux.Vars(r)
	if err != nil {
		WriteResp(w, http.StatusBadRequest, repr.NewErrorResp(err))
		return
	}
	versionModel := new(model.Version)
	tx := ctx.DB().Begin()
	// defer func() {
	// 	if err != nil {
	// 		tx.Rollback()
	// 	} else {
	// 		tx.Commit()
	// 	}
	// }()
	repo, err := model.NewRepo(tx, &model.Version{})
	err = repo.FindByID(vars["version_id"], versionModel)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		return
	}
	if versionModel.RelFile.String() != vars["file_id"] {
		tx.Rollback()
		WriteResp(w, http.StatusNotFound, repr.NewError("Version Not found"))
		return
	}
	newVersion := new(model.Version)
	err = input.ToModel(newVersion)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	log.Printf("Updating tags for version %s: %s", versionModel.ID.String(), input.Data.Tags)
	versionModel.Tags = newVersion.Tags
	err = repo.Save(versionModel)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	err = input.FromModel(versionModel)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	err = tx.Commit().Error
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	WriteResp(w, http.StatusOK, input)
}

// PostVersionContent -
func PostVersionContent(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	tx := ctx.DB().Begin()
	vars := mux.Vars(r)
	var err error
	var repo model.Repository

	repo, err = model.NewRepo(tx, &model.Version{})
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	version := new(model.Version)
	err = repo.FindByID(vars["version_id"], version)
	log.Print(version)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		return
	}
	if version.RelFile.String() != vars["file_id"] {
		tx.Rollback()
		WriteResp(w, http.StatusNotFound, repr.NewError("VersionNotFound"))
		return
	}
	if version.UploadedSize == version.Size {
		tx.Rollback()
		WriteResp(w, http.StatusConflict, repr.NewError("Version already uploaded"))
		return
	}
	offset := r.Header.Get("x-offset")
	log.Printf("Check offset %s", offset)
	if offset != fmt.Sprintf("%d", version.UploadedSize) {
		tx.Rollback()
		WriteResp(w, http.StatusBadRequest, repr.NewError("Incorrect offset"))
		return
	}
	null := uuid.UUID{}
	if version.StorageID == null {
		version.StorageID = uuid.NewV4()
	}
	val, err := strconv.Atoi(r.Header.Get("content-length"))
	log.Printf("Content Length ...%d", val)
	log.Printf("Remained len: %d", version.Size-version.UploadedSize)
	if err != nil || uint64(val) != version.Size-version.UploadedSize {
		tx.Rollback()
		WriteResp(w, http.StatusBadRequest, repr.NewError("Incorrect offset"))
		return
	}
	log.Printf("Appending...")
	n, err := ctx.Storage().Append(version.StorageID, 0, r.Body)
	if err != nil {
		tx.Rollback()
		ctx.Storage().Delete(version.StorageID)
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	log.Printf("N = %d", n)
	version.UploadedSize += uint64(n)
	if version.UploadedSize == version.Size {
		log.Printf("Completing versionID %s; ", version.ID)
		err = ctx.Storage().Complete(version.StorageID, version.SHA256Hash)
		if err != nil {
			tx.Rollback()
			ctx.Storage().Delete(version.StorageID)
			WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
			return
		}
		log.Printf("Completed version ID %s", version.ID)
		version.Verified = true
		if ctx.Converter().IsConvRequired(version) {
			conv, err := ctx.Converter().Convert(tx, version)
			if err != nil {
				log.Printf("REDIS ERROR: %s", err.Error())
				ctx.Storage().Delete(version.StorageID)
				tx.Rollback()
				WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
				return
			}
			log.Print(conv)
		}
	}
	err = repo.Save(version)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	err = tx.Commit().Error
	if err != nil {
		WriteResp(w, http.StatusInternalServerError, repr.NewErrorResp(err))
		return
	}
	w.Header().Set("x-offset", fmt.Sprintf("%d", version.UploadedSize))
	w.WriteHeader(http.StatusCreated)
}

// GetVersionContent -
func GetVersionContent(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	version := new(model.Version)
	vars := mux.Vars(r)
	tx := ctx.DB().Begin()
	repo, err := model.NewRepo(tx, version)
	if err != nil {
		tx.Rollback()
		w.WriteHeader(http.StatusNotFound)
		return
	}
	err = repo.FindByID(vars["version_id"], version)
	if err != nil {
		tx.Rollback()
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if !version.Verified {
		tx.Rollback()
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	file, err := ctx.Storage().ReadCloser(version.StorageID)
	if err != nil {
		tx.Rollback()
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	n, err := io.Copy(w, file)
	if err != nil {
		tx.Rollback()
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	tx.Commit()
	log.Printf("Wrote %d bytes of %s version", n, version.ID)
}

// DeleteVersionHandler -
func DeleteVersionHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fileID := vars["file_id"]
	versionID := vars["version_id"]
	tx := ctx.DB().Begin()
	version := new(model.Version)
	repo, _ := model.NewRepo(tx, version)
	err := repo.FindByID(versionID, version)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		return
	}
	if version.RelFile.String() != fileID {
		tx.Rollback()
		WriteResp(
			w, http.StatusNotFound,
			repr.NewError(fmt.Sprintf("Not found version %s for file %s", versionID, fileID)),
		)
		return
	}
	rep := new(repr.VersionMsg)
	err = rep.FromModel(version)
	if err != nil {
		tx.Rollback()
		WriteResp(w, http.StatusNotFound, repr.NewErrorResp(err))
		return
	}
	tx.Commit()
	WriteResp(w, http.StatusOK, rep)
}
