PACKAGES = common/utils common/config api/controller api/model api daemon
IMPORT_PATH = gitlab.com/atomikin/file-server

# versioning
export RELEASE ?= minor
export VERSION ?= $(shell build-helper build-version --increment -r "${RELEASE}")
export BRANCH ?= $(shell git rev-parse --abbrev-ref HEAD)
export RESULT_NAME ?= file-server

RUN_CFG ?= ./config.yml

export BUILD_DIR = ./build

all: save-version

app: libs
	go build  -o $(BUILD_DIR)/$(RESULT_NAME) $(IMPORT_PATH)

run: libs
	go run $(IMPORT_PATH) $(RUN_CFG)

libs: daemon api

build-dir:
	rm -rf ./build && mkdir ./build

common-config:
		go install $(IMPORT_PATH)/common/utils

common-utils: common-config
		go install $(IMPORT_PATH)/common/utils

api-controller: common-utils common-config context api-model
		go install $(IMPORT_PATH)/api/controller

api-representation:
		go install $(IMPORT_PATH)/api/representation

api-model: common-utils common-config
		go install $(IMPORT_PATH)/api/model

api: api-controller api-model api-representation storage context api-converter
		go install $(IMPORT_PATH)/api

api-converter: api-model common-config
	go install $(IMPORT_PATH)/api/converter

storage: common-config
		go install $(IMPORT_PATH)/storage

daemon: api context
		go install $(IMPORT_PATH)/daemon

context: common-config storage
	go install $(IMPORT_PATH)/context

test: test-storage

test-storage: storage
	go test $(IMPORT_PATH)/storage -run ''

container: app
	docker build . -t "192.168.1.58:5000/internal/file-server:${VERSION}"
	docker tag "192.168.1.58:5000/internal/file-server:${VERSION}" "192.168.1.58:5000/internal/file-server:latest"
	docker push "192.168.1.58:5000/internal/file-server:${VERSION}"
	docker push 192.168.1.58:5000/internal/file-server:latest

save-version: container
	build-helper build-version --save "${VERSION}"
	git add version.ini
	git commit -m "Compiled ${VERSION}. Release: ${RELEASE}"
	git push origin "${BRANCH}"

	
