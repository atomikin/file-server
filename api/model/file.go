package model

import (
	"fmt"
	"log"
	"math"
	"reflect"

	"github.com/jinzhu/gorm"
)

// File DB model
type File struct {
	Base
	Name     string    `gorm:"not null;unique_index:name_folder_index;" json:"name"`
	Folder   string    `gorm:"not null;unique_index:name_folder_index;" json:"folder"`
	Versions []Version `gorm:"foreignkey:RelFile" json:"versions"`
	Tags     []Tag     `gorm:"many2many:file_tags" json:"tags"`
}

// FileRepository -
type FileRepository struct {
	db *gorm.DB
}

// Files -
type Files []File

// Save - save file
func (repo *FileRepository) Save(fileObj interface{}) error {
	// tags
	file, ok := fileObj.(*File)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(fileObj).Name())
	}
	tags := file.Tags
	if repo.db.NewRecord(file) {
		if err := repo.db.Create(file).Error; err != nil {
			return err
		}
	} else {
		if err := repo.db.Save(file).Error; err != nil {
			return err
		}
	}
	if tags != nil {
		err := repo.db.Model(file).Association("Tags").Replace(tags).Error
		if err != nil {
			return err
		}
	}
	return nil
}

// FindByID -
func (repo *FileRepository) FindByID(id interface{}, target interface{}) error {
	return repo.db.Preload("Tags").Where("id = ?", id).First(target).Error
}

// FindBy for file
func (repo *FileRepository) FindBy(obj interface{}, pairs ...Arg) error {
	files, ok := obj.(*Files)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(obj).Name())
	}
	searchableFields := map[string]bool{
		"name":   true,
		"tags":   true,
		"id":     true,
		"folder": true,
	}
	db := repo.db
	for _, arg := range pairs {
		if _, ok := searchableFields[arg.Field]; !ok {
			continue
		}
		if arg.Field == "tags" {
			db = addSelectByTag(db, arg, "files", "file_tags", "file_id")
			continue
		}
		db = db.Where(arg.FieldOp(), arg.Value)
	}
	log.Print(db.QueryExpr())
	return db.Preload("Tags").Find(files).Error
}

// Delete -
func (repo *FileRepository) Delete(obj interface{}) error {
	if file, ok := obj.(*File); ok {
		versions := make(Versions, 0)
		versionsRepo, _ := NewRepo(repo.db, &Version{})
		err := versionsRepo.FindBy(versions, Arg{Field: "rel_file", Value: file.ID})
		if err != nil {
			return err
		}
		for _, version := range versions {
			err = versionsRepo.Delete(version)
			if err != nil {
				return err
			}
		}
		return repo.db.Delete(file).Error
	}
	return fmt.Errorf("Incorrect type %s", reflect.TypeOf(obj).Name())
}

// Exists -
func (repo *FileRepository) Exists(obj interface{}) bool {
	if file, ok := obj.(*File); ok {
		return !repo.db.NewRecord(file)
	}
	return false
}

// Slice - Slice interface impl
func (files *Files) Slice(from, to int) {
	max := len(*files)
	from = int(math.Min(float64(from), float64(max)))
	to = int(math.Min(float64(to), float64(max)))
	log.Printf("Slicing from %d to %d", from, to)
	*files = (*files)[from:to]
}

// Len - Slice interface impl
func (files *Files) Len() int {
	return len(*files)
}
