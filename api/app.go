package api

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/atomikin/file-server/api/controller"
	"gitlab.com/atomikin/file-server/api/model"
	"gitlab.com/atomikin/file-server/context"
)

type handlerType func(http.ResponseWriter, *http.Request)
type extHandlerType func(context.Context, http.ResponseWriter, *http.Request)

func registerHandlers(router *mux.Router, ctx context.Context) {
	wrapper := func(ctx context.Context, h extHandlerType) handlerType {
		return func(w http.ResponseWriter, r *http.Request) {
			h(ctx.Clone(), w, r)
		}
	}
	router.HandleFunc("/files", wrapper(ctx, controller.PostFileHandler)).
		Methods("POST")
	router.HandleFunc("/files/{file_id}", wrapper(ctx, controller.GetFileHandler)).
		Methods("GET")
	router.HandleFunc("/files", wrapper(ctx, controller.SearchFilesHandler)).
		Methods("GET")
	router.HandleFunc("/files/{file_id}", wrapper(ctx, controller.PutFileHandler)).
		Methods("PUT")
	router.HandleFunc("/files/{file_id}", wrapper(ctx, controller.DeleteFileHandler)).
		Methods("DELETE")
	router.HandleFunc("/files/{file_id}/versions", wrapper(ctx, controller.PostVersionHandler)).
		Methods("POST")
	router.HandleFunc("/files/{file_id}/versions/{version_id}", wrapper(ctx, controller.GetVersionHandler)).
		Methods("GET")
	router.HandleFunc("/files/{file_id}/versions", wrapper(ctx, controller.GetVersionsHandler)).
		Methods("GET")
	router.HandleFunc("/files/{file_id}/versions/{version_id}", wrapper(ctx, controller.PatchVersionHandler)).
		Methods("PATCH")
	router.HandleFunc("/files/{file_id}/versions/{version_id}/content", wrapper(ctx, controller.PostVersionContent)).
		Methods("POST")
	router.HandleFunc("/files/{file_id}/versions/{version_id}/content", wrapper(ctx, controller.GetVersionContent)).
		Methods("GET")

	router.HandleFunc("/conversions/{conv_id}", wrapper(ctx, controller.PatchConvertion)).
		Methods("PATCH")
	router.HandleFunc("/conversions/", wrapper(ctx, controller.GetConversions)).
		Methods("GET")
	router.HandleFunc("/conversions/{conv_id}", wrapper(ctx, controller.GetConversion)).
		Methods("GET")
	router.HandleFunc("/conversions/{conv_id}/restart", wrapper(ctx, controller.RestartConversion)).
		Methods("POST")
}

func migrate(ctx context.Context) {
	ctx.DB().AutoMigrate(&model.File{})
	ctx.DB().AutoMigrate(&model.Version{})
	ctx.DB().AutoMigrate(&model.Tag{})
	ctx.DB().AutoMigrate(&model.Conversion{})
}

/*
Run starts the API server and initializes all routes
*/
func Run(ctx context.Context) {
	router := mux.NewRouter()
	migrate(ctx)
	registerHandlers(router, ctx)
	srv := &http.Server{
		Handler:      router,
		Addr:         fmt.Sprintf("%s:%s", ctx.Cfg().Host, ctx.Cfg().Port),
		WriteTimeout: 300 * time.Second,
		ReadTimeout:  300 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
