package representation

import (
	"encoding/json"
	"io"
	"io/ioutil"
)

// Load -
func Load(target Representation, reader io.Reader) error {
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, target)
	if err != nil {
		return err
	}
	return nil
}

// Dump -
func Dump(target Representation) ([]byte, error) {
	return json.Marshal(target)
}

// BaseMsg -
type BaseMsg struct {
	Version int    `json:"version"`
	Type    string `json:"type"`
}

// BaseCollectionMsg -
type BaseCollectionMsg struct {
	BaseMsg
	Offset int `json:"offset"`
	Total  int `json:"total"`
	Count  int `json:"count"`
}

// Representation -
type Representation interface {
	FromModel(target interface{}) error
	ToModel(target interface{}) error
	Verify() error
}
