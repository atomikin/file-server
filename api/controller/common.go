package controller

import (
	"log"
	"net/http"

	repr "gitlab.com/atomikin/file-server/api/representation"
)

// WriteResp - Common Error response
func WriteResp(w http.ResponseWriter, status int, msg repr.Representation) {
	log.Printf("Resp Code: %d", status)
	w.Header().Set("content-type", "application/json")
	data, err := repr.Dump(msg)
	if err != nil {
		log.Printf("Faced error during response sending: %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		data, _ := repr.Dump(repr.NewErrorResp(err))
		w.Write(data)
		return
	}
	w.WriteHeader(status)
	w.Write(data)
}
