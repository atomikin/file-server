#!/usr/bin/env bash

set -exuo pipefail

CONN_URI="${CONN_URI:-postgresql://fileserver:${PASSWORD}@postgres:5433/fileserver}"
THIS_DIR=$(dirname $0)
THIS_DIR=$(cd ${THIS_DIR} && pwd)
cmd="psql ${CONN_URI} -f"
${cmd} sql/1.psql
