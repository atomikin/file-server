package converter

import (
	goCtx "context"
	"log"
	"path/filepath"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/jinzhu/gorm"
	"gitlab.com/atomikin/file-server/api/model"
	repr "gitlab.com/atomikin/file-server/api/representation"
	"gitlab.com/atomikin/file-server/common/config"
)

const (
	convertedTag = "converted"
)

// Converter -
type Converter interface {
	Convert(*gorm.DB, *model.Version) (*model.Conversion, error)
	Restart(*model.Conversion) error
	IsConvRequired(*model.Version) bool
	Config() *config.ConverterConf
}

// Queue -
type Queue interface {
	Push(repr.Representation) error
	PopWait() []string
	CheckConn() error
}

// MediaConverter -
type MediaConverter struct {
	queue Queue
	cfg   *config.ConverterConf
	lock  sync.Mutex
}

// NewConverter -
func NewConverter(cfg *config.ConverterConf) Converter {
	return &MediaConverter{queue: NewRedisFIFOQueue(cfg.Addr, cfg.ConvQueue), cfg: cfg}
}

// RedisFIFOQueue -
type RedisFIFOQueue struct {
	redisClient *redis.Client
	key         string
}

// NewRedisFIFOQueue -
func NewRedisFIFOQueue(addr, key string) Queue {
	client := redis.NewClient(&redis.Options{Addr: addr, Password: "", DB: 0})
	return &RedisFIFOQueue{redisClient: client, key: key}
}

func (q *RedisFIFOQueue) client() *redis.Client {
	cont, cancel := goCtx.WithDeadline(goCtx.Background(), time.Now().Add(2*time.Second))
	defer cancel()
	_, err := q.redisClient.Ping(cont).Result()
	if err != nil {
		log.Fatal(err.Error())
	}
	return q.redisClient
}

// CheckConn - ping server and return error if any
func (q *RedisFIFOQueue) CheckConn() error {
	ctx, cancel := goCtx.WithTimeout(goCtx.Background(), 50*time.Millisecond)
	defer cancel()
	_, err := q.client().Ping(ctx).Result()
	// log.Printf("REDIS ERROR: %s", err.Error())
	return err
}

// Push -
func (q *RedisFIFOQueue) Push(rep repr.Representation) error {
	// ctx := context.Background()
	ctx, cancel := goCtx.WithTimeout(goCtx.Background(), 10*time.Second)
	defer cancel()
	value, err := repr.Dump(rep)
	if err != nil {
		return err
	}
	_, err = q.client().LPush(ctx, q.key, value).Result()
	if err != nil {
		return err
	}
	return nil
}

// PopWait - RAW impl
func (q *RedisFIFOQueue) PopWait() []string {
	for {
		msgs, err := q.client().BRPop(goCtx.Background(), 10*time.Minute, q.key).Result()
		if err != nil {
			time.Sleep(1 * time.Second)
			continue
		}
		return msgs
	}
}

// Config -
func (cv *MediaConverter) Config() *config.ConverterConf {
	return cv.cfg
}

func setExt(filename, newExt string) string {
	extLen := len(filepath.Ext(filename))
	if string(newExt[0]) != "." {
		newExt = "." + newExt
	}
	return filename[0:len(filename)-extLen] + newExt
}

func shouldConvert(filename string, extensions []string) bool {
	thisExt := filepath.Ext(filename)
	for _, ext := range extensions {
		if thisExt == ext {
			return true
		}
	}
	return false
}

// IsConvRequired -
func (cv *MediaConverter) IsConvRequired(version *model.Version) bool {
	for _, tag := range version.Tags {
		if tag.Name == convertedTag {
			return false
		}
	}
	return true
}

func (cv *MediaConverter) ensureConnIsOK() {
	if err := cv.queue.CheckConn(); err != nil {
		log.Printf("REDIS ERROR: %s", err.Error())
		cv.lock.Lock()
		defer cv.lock.Unlock()
		if err = cv.queue.CheckConn(); err != nil {
			cv.queue = NewRedisFIFOQueue(cv.Config().Addr, cv.Config().ConvQueue)
			log.Printf("REDIS ERROR: Reconnected...")
		}
	}
}

// Convert - if transactional behavior is needed, in should be initiated elsewhere
func (cv *MediaConverter) Convert(db *gorm.DB, version *model.Version) (*model.Conversion, error) {
	cv.ensureConnIsOK()
	if !shouldConvert(version.FileName, cv.Config().ConvertExt) {
		return nil, nil
	}
	conv := new(model.Conversion)
	repo, err := model.NewRepo(db, conv)
	if err != nil {
		return nil, err
	}
	conv.SrcVersionID = version.ID
	conv.NewName = setExt(version.FileName, cv.Config().TargetContainerExt)
	conv.Preset = cv.Config().DefaultPreset
	err = repo.Save(conv)
	if err != nil {
		return nil, err
	}
	conv.SrcVersion = *version
	rep := new(repr.ConversionMsg)
	if err = rep.FromModel(conv); err != nil {
		return nil, err
	}
	err = cv.queue.Push(rep)
	if err != nil {
		return nil, err
	}
	return conv, nil
}

// Restart - restart a conv
func (cv *MediaConverter) Restart(conv *model.Conversion) error {
	cv.ensureConnIsOK()
	rep := new(repr.ConversionMsg)
	if err := rep.FromModel(conv); err != nil {
		return err
	}
	return cv.queue.Push(rep)
}
