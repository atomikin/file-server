CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

create table base (
    id uuid primary key DEFAULT uuid_generate_v4(),
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp
);

-- files table
create table files (
    name text not null,
    folder text not null,
    unique(name, folder),
    primary key(id)
) inherits (base);


create index on files(name);
create index on files(folder);
-- create file versions
create table versions (
    file_name text not null,
    storage_id UUID,
    file_id uuid references files(id) on delete cascade,
    sha256 text not null,
    size integer not null,
    uploaded_size integer default 0,
    verified bool default false,
    primary key(id)
) inherits (base);

create index on versions(file_name);
create index on versions(storage_id);
create index on versions(sha256);

create table tags (
    id serial primary key,
    name text
);

create table files_tags (
    file_id uuid REFERENCES files(id) ON DELETE CASCADE,
    tag_id INTEGER REFERENCES tags(id) ON DELETE CASCADE
);

create table versions_tags (
    version_id uuid REFERENCES versions(id) ON DELETE CASCADE,
    tag_id INTEGER REFERENCES tags(id) ON DELETE CASCADE
);

create type conversion_status_type as enum (
    'created',
    'in_pregress',
    'success',
    'error'
);

create table conversions(
    src_version_id uuid references versions(id) ON DELETE CASCADE,
    result_version_id uuid references versions(id) ON DELETE CASCADE,
    progress integer not null default 0,
    new_name text not null,
    preset text not null default 'HQ 1080p30 Surround'::text,
    status conversion_status_type not null default 'created'::conversion_status_type,
    unique(src_version_id, preset),
    primary key(id)
) inherits (base);

create index on conversions(new_name);