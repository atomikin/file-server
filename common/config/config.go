package config

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

// SupportedBrockers -
var SupportedBrockers = map[string]bool{"redis": true}

// SupportedDBs -
var SupportedDBs = map[string]bool{"postgres": true, "sqlite3": true}

/*
DBConf is a database related part of the config
*/
type DBConf struct {
	URI      string `yaml:"uri"`
	Dialect  string `yaml:"dialect"`
	DebugLog bool   `yaml:"debug_log"`
}

/*
APIConf - API config of the app
*/
type APIConf struct {
	Host         string `yaml:"host"`
	Port         string `yaml:"port"`
	MaxChunkSize uint   `yaml:"max_chunk_size"`
}

/*
StorageConf - storage configuration
*/
type StorageConf struct {
	Path string `yaml:"path"`
}

//ConverterConf -
type ConverterConf struct {
	QueueType          string   `yaml:"queue_type"`
	Addr               string   `yaml:"addr"`
	ConvertExt         []string `yaml:"convert_ext"`
	TargetContainerExt string   `yaml:"target_container_ext"`
	DefaultPreset      string   `yaml:"default_preset"`
	ConvQueue          string   `yaml:"conv_queue"`
}

/*
Configuraton is an object representation of the application config JSON
*/
type Configuraton struct {
	API       APIConf       `yaml:"api"`
	Storage   StorageConf   `yaml:"storage"`
	DB        DBConf        `yaml:"db"`
	Converter ConverterConf `yaml:"converter"`
}

// Verify - config veriifcation
func (cfg *Configuraton) Verify() error {
	if _, ok := SupportedDBs[cfg.DB.Dialect]; !ok {
		return fmt.Errorf("Wrong dialect: %s", cfg.DB.Dialect)
	}
	if _, ok := SupportedBrockers[cfg.Converter.QueueType]; !ok {
		return fmt.Errorf("Wrong broker type: %s", cfg.Converter.QueueType)
	}
	if cfg.Converter.DefaultPreset == "" {
		return errors.New("Converter default_preset is required")
	}
	if cfg.Converter.Addr == "" {
		return errors.New("converter host and port are required")
	}
	return nil
}

/*
Load configuration file
*/
func Load(path string) *Configuraton {
	file, err := os.Open(path)
	if err != nil {
		log.Fatalf("Config file %s does not exist: %s", path, err.Error())
	}
	defer file.Close()
	data, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("Error reading config: %s", err.Error())
	}
	con := new(Configuraton)
	err = yaml.Unmarshal(data, con)
	if err != nil {
		log.Fatalf("Incorrect config: %s", err.Error())
	}
	err = con.Verify()
	if err != nil {
		log.Fatal(err.Error())
	}
	return con
}
