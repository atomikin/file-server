package model

import (
	"fmt"
	"log"
	"math"
	"reflect"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

// Version DB model
type Version struct {
	Base
	FileName     string    `gorm:"not null;index"`
	StorageID    uuid.UUID `gorm:"type:uuid;index;"`
	RelFile      uuid.UUID `gorm:"type:uuid;not null;index;"`
	SHA256Hash   string    `gorm:"not null" json:"sha256"`
	Size         uint64    `gorm:"not null" json:"size"`
	UploadedSize uint64    `gorm:"not null" json:"uploaded_size"`
	Verified     bool      `gorm:"default:false" json:"verified"`
	Tags         []Tag     `gorm:"many2many:version_tags"`
}

// VersionRepository -
type VersionRepository struct {
	db *gorm.DB
}

// Versions -
type Versions []Version

// Save - save file
func (repo *VersionRepository) Save(verObj interface{}) error {
	// tags
	version, ok := verObj.(*Version)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(verObj).Name())
	}
	tags := version.Tags
	if repo.db.NewRecord(version) {
		if err := repo.db.Create(version).Error; err != nil {
			return err
		}
	} else {
		if err := repo.db.Save(version).Error; err != nil {
			return err
		}
	}
	if tags != nil {
		err := repo.db.Model(version).Association("Tags").Replace(tags).Error
		if err != nil {
			return err
		}
	}
	return nil
}

// FindByID -
func (repo *VersionRepository) FindByID(id interface{}, target interface{}) error {
	return repo.db.Preload("Tags").Where("id = ?", id).First(target).Error
}

// FindBy for file
func (repo *VersionRepository) FindBy(obj interface{}, pairs ...Arg) error {
	versions, ok := obj.(*Versions)
	if !ok {
		return fmt.Errorf("Incorrect type %s", reflect.TypeOf(obj).Name())
	}
	searchableFields := map[string]bool{
		"file_name":   true,
		"tags":        true,
		"id":          true,
		"rel_file":    true,
		"sha256_hash": true,
	}
	log.Print(pairs)
	db := repo.db.New()
	for _, arg := range pairs {
		if _, ok := searchableFields[arg.Field]; !ok {
			continue
		}
		if arg.Value == "" {
			continue
		}
		if arg.Field != "tags" {
			db = addSelectBy(repo.db, arg)
		} else {
			db = addSelectByTag(repo.db, arg, "versions", "version_tags", "version_id")
		}
	}
	return db.Preload("Tags").Order("created_at DESC").Find(versions).Error
}

// Delete -
func (repo *VersionRepository) Delete(obj interface{}) error {
	if version, ok := obj.(*Version); ok {
		return repo.db.Delete(version).Error
	}
	return fmt.Errorf("Incorrect type %s", reflect.TypeOf(obj).Name())
}

// Exists -
func (repo *VersionRepository) Exists(obj interface{}) bool {
	if version, ok := obj.(*Version); ok {
		return !repo.db.NewRecord(version)
	}
	return false
}

// Slice - Slice interface impl
func (versions *Versions) Slice(from, to int) {
	max := len(*versions)
	from = int(math.Min(float64(from), float64(max)))
	to = int(math.Min(float64(to), float64(max)))
	log.Printf("Slicing from %d to %d", from, to)
	*versions = (*versions)[from:to]
}

// Len - Slice interface impl
func (versions *Versions) Len() int {
	return len(*versions)
}
